//
//  HeroDetailViewController.swift
//  VA_SuperHeroes
//
//  Created by Vikash Anand on 30/10/19.
//  Copyright © 2019 Vikash Anand. All rights reserved.
//

import UIKit

class HeroDetailViewController: UIViewController {

    var iconName: String?
    var name: String?
    
    @IBOutlet private var iconImageView: UIImageView!
    @IBOutlet private var nameLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let iconName = self.iconName, let image = UIImage(named: iconName) {
            self.iconImageView.image = image
        }
        
        if let name = self.name {
            self.nameLabel.text = name
        }
    }
}


