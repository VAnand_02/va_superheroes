//
//  HeroTableViewCell.swift
//  VA_SuperHeroes
//
//  Created by Vikash Anand on 30/10/19.
//  Copyright © 2019 Vikash Anand. All rights reserved.
//

import UIKit

class HeroTableViewCell: UITableViewCell {

    @IBOutlet var iconImageView: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var infoButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.iconImageView.backgroundColor = UIColor.white
        self.iconImageView.layer.cornerRadius = self.iconImageView.bounds.width / 2.0
        self.iconImageView.layer.borderColor = UIColor.systemIndigo.cgColor
        self.iconImageView.layer.borderWidth = 2.0
    }
    
    func configiure(with iconName: String, and name: String, at indexPath: IndexPath) {
        if let image = UIImage(named: iconName) {
            self.iconImageView.image = image
        }
        self.nameLabel.text = name
        self.infoButton.tag = indexPath.row
    }
}
