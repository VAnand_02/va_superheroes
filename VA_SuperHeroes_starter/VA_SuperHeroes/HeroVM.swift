//
//  HeroVM.swift
//  VA_SuperHeroes
//
//  Created by Vikash Anand on 30/10/19.
//  Copyright © 2019 Vikash Anand. All rights reserved.
//

import Foundation

final class HeroVM {
    
    static private var heroDetailsUrl: [String]  = {
        let ironManUrl = "https://www.marvel.com/characters/iron-man-tony-stark"
        let hulkUrl = "https://www.marvel.com/characters/hulk-bruce-banner"
        let blackWidowManUrl = "https://www.marvel.com/characters/black-widow-natasha-romanoff"
        let spiderManUrl = "https://www.marvel.com/characters/spider-man-peter-parker"
        let thorUrl = "https://www.marvel.com/characters/thor-thor-odinson"
        
        return [ironManUrl, hulkUrl, blackWidowManUrl, spiderManUrl, thorUrl]
    }()
    
    static private var heroes: [Hero]  = {
        let ironMan = Hero(name: "ironMan", universeName: "MCU", imageIconName: "ironMan")
        let hulk = Hero(name: "hulk", universeName: "MCU", imageIconName: "hulk")
        let spiderMan = Hero(name: "spiderMan", universeName: "MCU", imageIconName: "spiderMan")
        let blackWidow = Hero(name: "blackWidow", universeName: "MCU", imageIconName: "blackWidow")
        let thor = Hero(name: "thor", universeName: "MCU", imageIconName: "thor")
        
        return [ironMan, hulk, spiderMan, blackWidow, thor]
    }()
    
    static private subscript(modelIndex: Int) -> Hero {
        get {
            return self.heroes[modelIndex]
        }
    }
    
    static subscript(urlIndex: Int) -> String {
        get {
            return self.heroDetailsUrl[urlIndex]
        }
    }
    
    static func count() -> Int {
        self.heroes.count
    }
    
    static func iconName(for index: Int) -> String {
        self.heroes[index].imageIconName
    }
      
    static func name(for index: Int) -> String {
        self.heroes[index].name
    }
}
